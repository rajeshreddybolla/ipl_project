fetch('./output/extraRun.json')
  .then((response) => response.json())
  .then((data) => genericGraph(data));


function genericGraph(extraRun) {
  Highcharts.chart('container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Extra Runs Conceeded Per Team In Year 2015',
    },
    xAxis: {
      categories: Object.keys(extraRun),
    },
    yAxis: {
      title: {
        text: 'Extra Runs Conceeded'
      }
    },
    series: [{
      name: '2015',
      data: Object.values(extraRun),

    }]
  });
}

fetch('./output/matchesPerYear.json')
  .then((response) => response.json())
  .then((data) => genericGraph2(data));

function genericGraph2(extraRun) {
  Highcharts.chart('container2', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Total Number Of Matches Per Year',
    },
    xAxis: {
      title: {
        text: 'Years'
      },
      categories: Object.keys(extraRun),
    },
    yAxis: {
      title: {
        text: 'No.of Matches'
      }
    },
    series: [{
      name: 'No.of Matches',
      data: Object.values(extraRun),

    }]
  });
}

fetch("./output/economicalBowler.json")
  .then((response) => response.json())
  .then((data) => Graph(data));

function Graph(economicalBowler) {
  Highcharts.chart('container3', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Top 10 Economical Bowlers in 2015'
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    },
    yAxis: {
      title: {
        text: 'Economy'
      }
    },
    tooltip: {
      pointFormat: 'Economy of Bowler in 2015: <b>{point.y:.1f}</b>'
    },
    series: [{
      name: 'Bowlers',
      data: economicalBowler,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y:.1f}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    }]
  });

}


fetch('./output/numberofwinnings.json')
  .then((response) => response.json())
  .then((data) => Graph2(data));

function Graph2(numberofwinnings) {
  function initObj(obj) {
      Object.keys(numberofwinnings).forEach(season=>{
          obj[season] = 0;
      }
      );
  }

  let series = Object.entries(numberofwinnings).reduce((result,year)=>{
      let season = year[0];
      let teams = year[1];
      Object.entries(teams).forEach(array=>{
          let team = array[0];
          let MatchWon = array[1];
          if (result[team] === undefined) {
              let obj = {};
              initObj(obj);
              result[team] = obj;
          }
          result[team][season] = MatchWon;
      }
      );
      return result;
  }
  , {});

  series = Object.entries(series).map(team=>{
      let teamName = team[0];
      let teamData = Object.values(team[1]);
      return {
          'name': teamName,
          'data': teamData
      };
  }
  );

  Highcharts.chart('container4', {
      chart: {
          type: 'line'
      },
      title: {
          text: 'Matches Won Per Team Per Year'
      },
      subtitle: {
          text: 'IPL'
      },
      xAxis: {
          categories: Object.keys(numberofwinnings)
      },
      yAxis: {
          title: {
              text: 'Matches'
          }
      },
      series: series
  });

}
