let ipl = require('./ipl');
let converter = require('convert-csv-to-json');
const fs = require("fs");
const path = require("path");


const matches_path = path.resolve(__dirname, "../data/matches.csv");
const deliveries_path = path.resolve(__dirname, "../data/deliveries.csv");


let matches = converter.fieldDelimiter(',').getJsonFromCsv(matches_path);
let deliveries = converter.fieldDelimiter(',').getJsonFromCsv(deliveries_path);


let matchesPerYear = ipl.getMatchesPerYear(matches);
console.log(matchesPerYear);
writeFile(
    path.resolve(__dirname, "../public/output/matchesPerYear.json"),
    JSON.stringify(matchesPerYear, null, 2)
);



let numberofwinnings = ipl.matchesWon(matches);
console.log(numberofwinnings);
writeFile(
    path.resolve(__dirname, "../public/output/numberofwinnings.json"),
    JSON.stringify(numberofwinnings, null, 2)
);


let extraRun = ipl.extraruns(matches,deliveries, 2015);
console.log(extraRun);
writeFile(
    path.resolve(__dirname, "../public/output/extraRun.json"),
    JSON.stringify(extraRun, null, 2)
);


let economicalBowler = ipl.economicalBowler(matches,deliveries);
console.log(economicalBowler);
writeFile(
    path.resolve(__dirname, "../public/output/economicalBowler.json"),
    JSON.stringify(economicalBowler, null, 2)
);


let bothTossAndMatchWinner = ipl.winningBothTossAndMatch(matches);
console.log(bothTossAndMatchWinner);
writeFile(
    path.resolve(__dirname, "../public/output/bothTossAndMatchWinner.json"),
    JSON.stringify(bothTossAndMatchWinner, null, 2)
);


let strikeRate = ipl.strikeRate(matches,deliveries);
console.log(strikeRate);
writeFile(
    path.resolve(__dirname, "../public/output/strikeRate.json"),
    JSON.stringify(strikeRate, null, 2)
);

let playerOftheMatch = ipl.playerOftheMatch(matches);
console.log(playerOftheMatch)
writeFile(
    path.resolve(__dirname, "../public/output/playerOftheMatch.json"),
    JSON.stringify(playerOftheMatch , null, 2)
);

let economicalBowlerInSperOver = ipl.economicalBowlerInSperOver(deliveries);
console.log(economicalBowlerInSperOver)
writeFile(
    path.resolve(__dirname, "../public/output/economicalBowlerInSperOver.json"),
    JSON.stringify(economicalBowlerInSperOver, null, 2)
);




function writeFile(path, data) {
    fs.writeFile(path, data, function (err) {
        if (err) {
            console.log(err);
        }
    });
}

