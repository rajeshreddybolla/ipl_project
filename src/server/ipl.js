var _ = require('lodash')
function getMatchesPerYear(matches) {
    let result = {};

    for (let i = 0; i < matches.length; i++) {
        let season = matches[i].season;
        if (result.hasOwnProperty(season)) {
            result[season] += 1;
        } else {
            result[season] = 1;
        }

    }
    return result;
}


function matchesWon(matches) {
    let winners = {};
    matches.forEach(function (arrayItem){
        let team = arrayItem.winner;
        let year = arrayItem.season;
        if(winners.hasOwnProperty(year)){
            if (!winners[year].hasOwnProperty(team)) {
                        winners[year][team] = 1
                    } else {
                        winners[year][team] += 1
                    }
        }else{
            winners[year] = {}
        }
    }); 
   
    return winners;
}



function extraruns(matches, deliveries, year) {

    var extraRuns = {};
    var matchID = [];

    for (let object in matches) {
        if (matches[object]["season"] == year) {
            matchID.push(matches[object]["id"]);
        }
    }

    deliveries.forEach(function (arrayItem) {

        if (matchID.includes(arrayItem.match_id)) {
            if (!extraRuns.hasOwnProperty(arrayItem.bowling_team)) {
                extraRuns[arrayItem.bowling_team] = parseInt(arrayItem.extra_runs);
            }
            else {
                extraRuns[arrayItem.bowling_team] += parseInt(arrayItem.extra_runs);
            }

        }
    });

    return extraRuns;

}


function economicalBowler(matches, deliveries) {

    var econ = {};
    var matchId2015 = [];
    let result = [];

    for (let object in matches) {
        if (matches[object]["season"] == 2015) {
            matchId2015.push(matches[object]["id"]);
        }
    }
    var overs = {};

    deliveries.forEach(function (arrayItem) {

        if (matchId2015.includes(arrayItem.match_id)) {
            if (!econ.hasOwnProperty(arrayItem.bowler)) {
                econ[arrayItem.bowler] = parseInt(arrayItem.total_runs);
            }
            else {
                econ[arrayItem.bowler] += parseInt(arrayItem.total_runs);
            }
            if (!overs.hasOwnProperty(arrayItem.bowler)) {
                overs[arrayItem.bowler] = 1;
            }
            else {
                overs[arrayItem.bowler] += 1;
            }

        }
    });

    // console.log(overs);
    // console.log(econ);
    for (let key in econ) {
        econ[key] = Math.round((econ[key] / (overs[key] / 6)) * 100) / 100;
    }


    var economy = Object.keys(econ).map(function (key) {
        return [key, econ[key]];
    }).sort(function (a, b) { return a[1] - b[1]; });

    for (var i = 0; i < 10; i++) {
        result[i] = economy[i];
    }
    // const economy = Object.fromEntries(
    //     Object.entries(econ).sort(([,a],[,b]) => a-b)
    // );
    //   return economy;
    return result
}


function winningBothTossAndMatch(matches) {
    let teamTossWinner = [];
    let matchWinner = [];
    let commonTeams = [];
    let result = {};
    for (i = 0; i < matches.length; i++) {
        teamTossWinner[i] = matches[i].toss_winner;
        matchWinner[i] = matches[i].winner;
        if (teamTossWinner[i] === matchWinner[i]) {
            commonTeams.push(matchWinner[i])
        }
    }

    commonTeams.forEach(function (i) { result[i] = (result[i] || 0) + 1; });


    return result;
}


function strikeRate(matches, deliveries) {

    let strike = {};

    var yearOfObjects = matches.map(item => item.season)
        .filter((value, index, self) => self.indexOf(value) === index)
    for (var i = 0; i < yearOfObjects.length; i++) {
        year = yearOfObjects[i];
        var batsManRuns = {};
        var matchID = [];
        for (let object in matches) {
            if (matches[object]["season"] == year) {
                matchID.push(matches[object]["id"]);
            }
        }


        let balls = {};
        deliveries.forEach(function (arrayItem) {

            if (matchID.includes(arrayItem.match_id)) {
                if (!batsManRuns.hasOwnProperty(arrayItem.batsman)) {
                    batsManRuns[arrayItem.batsman] = parseInt(arrayItem.batsman_runs);
                }
                else {
                    batsManRuns[arrayItem.batsman] += parseInt(arrayItem.batsman_runs);
                }

                if (!balls.hasOwnProperty(arrayItem.batsman)) {
                    balls[arrayItem.batsman] = 1
                }
                else {
                    balls[arrayItem.batsman] += 1;
                }

            }
        });
        for (key in batsManRuns) {
            if (strike.hasOwnProperty(key)) {
                strike[key][year] = Math.round((batsManRuns[key] * 100) / balls[key] * 100) / 100;
            } else {
                strike[key] = {};
                strike[key][year] = Math.round((batsManRuns[key] * 100) / balls[key] * 100) / 100;
            }

        }
    }

    // console.log(batsManRuns)
    // console.log(balls)
    return strike;

}

function playerOftheMatch(matches) {
    var yearOfObjects = matches.map(item => item.season)
        .filter((value, index, self) => self.indexOf(value) === index);
    var data = {};
    for (var i = 0; i < yearOfObjects.length; i++) {
        var year = yearOfObjects[i]
        var matchesDataOfYear = _.filter(
            matches, function (objectOfMatch) {
                return objectOfMatch.season == yearOfObjects[i];
            }
        );
        var countData = _.countBy(matchesDataOfYear, 'player_of_match')
        const sortedEntriesByVal = Object.entries(countData).sort(([, v1], [, v2]) => v1 - v2);
        data[year] = {}
        data[year][sortedEntriesByVal[sortedEntriesByVal.length - 1][0]] = {}
        data[year][sortedEntriesByVal[sortedEntriesByVal.length - 1][0]] = sortedEntriesByVal[sortedEntriesByVal.length - 1][1]
    }
    return (data);
}

function economicalBowlerInSperOver(deliveries) {

    let balls = {};
    let runs = {};


    deliveries.forEach(function (arrayItem) {
        if (arrayItem.is_super_over != 0) {
            if (balls.hasOwnProperty(arrayItem.bowler)) {
                balls[arrayItem.bowler] += 1
            } else {
                balls[arrayItem.bowler] = 1
            }

            if (!runs.hasOwnProperty(arrayItem.bowler)) {
                runs[arrayItem.bowler] = parseInt(arrayItem.total_runs)
            } else {
                runs[arrayItem.bowler] += parseInt(arrayItem.total_runs)
            }

        }
    });
    let economicalBowler = {};
    for (key in runs) {
        economicalBowler[key] = Math.round((runs[key] / (balls[key] / 6)) * 100) / 100;
    }

    //  console.log('runs==', runs)
    // console.log('balls==',balls);
    economicalBowlers = Object.keys(economicalBowler).map(function (key) {
        return [key, economicalBowler[key]];
    }).sort(function (a, b) { return a[1] - b[1]; });
    return economicalBowlers[0]
}


module.exports = {
    getMatchesPerYear,
    matchesWon,
    extraruns,
    economicalBowler,
    winningBothTossAndMatch,
    strikeRate,
    playerOftheMatch,
    economicalBowlerInSperOver,
    // playerDismissed,
};

